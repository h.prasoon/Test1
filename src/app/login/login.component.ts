import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { LoginService } from './login.service';
import { AuthService } from '../auth/auth.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css'],
  providers: [LoginService,AuthService]
})
export class LoginComponent implements OnInit {

  constructor(private router: Router, private loginService: LoginService,
    public authService: AuthService) { }

  ngOnInit() {
  }

  public login() {
    // alert('logged in succssfully');
    var s = this.loginService.Login();

    if(s == "true")
    {
      this.router.navigate(['/home']);
    }
  }
}