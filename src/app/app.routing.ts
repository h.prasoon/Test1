import { Routes, RouterModule } from '@angular/router';

import { LoginComponent } from './login/login.component';
import { HomeComponent } from './home/home.component';
import { CallbackComponent } from './callback.component';
import { ForgetComponent } from './forget/forget.component';
import { UsersComponent } from './users/users.component';
import{RolesComponent}from './roles/roles.component';

const appRoutes: Routes = [
    { path: '', component: LoginComponent},
    { path: 'login', component: LoginComponent },
    { path: 'home', component: HomeComponent },
	{ path: 'callback', component: CallbackComponent },
	{ path: 'forget', component: ForgetComponent },
    { path: 'users', component: UsersComponent },
    {path:'roles',component:RolesComponent},
    // otherwise redirect to home
    { path: '**', redirectTo: '' }
];

export const routing = RouterModule.forRoot(appRoutes);