import { Component, OnInit } from '@angular/core';
import { role } from './role';
import { FormControl, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-roles',
  templateUrl: './roles.component.html',
  styleUrls: ['./roles.component.css']
})
export class RolesComponent implements OnInit {

  Roles:role[];
  roleId:number;
  roleName:string;
  isEdit:boolean=false;


  constructor() {
  	this.Roles = [{id:1,name: "Owner"},{id:2,name: "Editor"},
            {id:3,name: "Viewer"},{id:4,name: "Presenter"}];
            this.roleId=0;
            this.roleName="";
            this.isEdit=false;
   }

  ngOnInit() {
  }
  newRole(){
  	this.roleId = 0;
 	this.roleName = "";
 	this.isEdit = false;
  }
  deleteRole(roleId)
  {
    var index = this.Roles.findIndex(r=>r.id== roleId);
    if(index>=0){
  	this.Roles.splice(index,1);}
  }

  addRole() 
  {
  	if(this.isEdit == false)
  	{
	 	var rl =
	 	{
	 	 	id : this.roleId,
	 		name : this.roleName
		};

		this.Roles.push(rl);
  	}
  	else
  	{
  		for (var i = 0; this.Roles.length > i; i += 1) {
                if (this.Roles[i].id === this.roleId) {
                    this.Roles[i].name = this.roleName;
                }
            }
  	}

  	this.roleId = 0;
 	this.roleName = "";
 	this.isEdit = false;
   }	

  loadRoleToEdit(id2,name2)
  {
	this.roleId = id2;
	this.roleName = name2;
	this.isEdit = true;
  }
}
