import { Component, OnInit } from '@angular/core';
import { user } from './user';
import { FormControl, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.css']
})
export class UsersComponent implements OnInit {
  Users : user[];
  id1 : number;
  name1 : string;
  isedit : boolean = false;
  
  constructor() { 
  	this.Users = [{id:0,name: "4"},{id:1,name: "5"},{id:2,name: "6"},
  				  {id:3,name: "7"},{id:4,name: "8"},{id:5,name: "9"}];
  	// this.usr=new user();
  }

  ngOnInit() {
  }

  deleteUser(userId)
  {
    var index = this.Users.findIndex(u=>u.id==userId);
    if(index>=0){
  	this.Users.splice(index,1);}
  }

  addUser(id1,name1) 
  {
  	if(this.isedit == false)
  	{
	 	var usr1 =
	 	{
	 	 	id : id1,
	 		name : name1
		};

		this.Users.push(usr1);
  	}
  	else
  	{
  		for (var i = 0; this.Users.length > i; i += 1) {
                if (this.Users[i].id === this.id1) {
                    this.Users[i].name = this.name1;
                }
            }
  	}

  	this.id1 = 0;
 	this.name1 = "";
 	this.isedit = false;
   }	

  loadUserToEdit(id2,name2)
  {
	this.id1 = id2;
	this.name1 = name2;
	this.isedit = true;
  }
}