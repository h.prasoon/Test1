import { NgModule }      from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule }    from '@angular/forms';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';

import { routing } from './app.routing';

import { AppComponent } from './app.component';
import { LoginComponent } from './login/login.component';
import { HomeComponent } from './home/home.component';
import { CallbackComponent } from './callback.component';
import { AuthService } from './auth/auth.service';
import { ForgetComponent } from './forget/forget.component';
import { NavbarComponent } from './navbar/navbar.component';
import { UsersComponent } from './users/users.component';
import { RolesComponent } from './roles/roles.component';

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    HomeComponent,
    CallbackComponent,
    ForgetComponent,
    NavbarComponent,
    UsersComponent,
    RolesComponent
  ],
  imports: [
    BrowserModule,
        FormsModule,
        HttpClientModule,
        routing
  ],
  providers: [AuthService],
  bootstrap: [AppComponent]
})
export class AppModule { }
