interface AuthConfig {
  CLIENT_ID: string;
  CLIENT_DOMAIN: string;
  AUDIENCE: string;
  REDIRECT: string;
  SCOPE: string;
}

export const AUTH_CONFIG: AuthConfig = {
  CLIENT_ID: 'haN1jyCWM00tGta1CBRnfvNZuUJGVJRd',
  CLIENT_DOMAIN: '9846114571.auth0.com', // e.g., you.auth0.com
  AUDIENCE: 'http://localhost:3001', // e.g., http://localhost:3001
  REDIRECT: 'http://localhost:4200/callback',
  SCOPE: 'h.prasoon@gmail.com'
};
